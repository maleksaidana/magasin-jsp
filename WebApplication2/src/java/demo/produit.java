/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;
/**
 *
 * @author Karim
 */
public class produit {
    
   private int id;
   private String nom;
   private int categorie;
   private double prix;
   private String desc;
   private int qty;
   
   public produit(int id, String nom,int categorie,double prix,String desc,int qty){
   
    this.id=id;
    this.nom=nom;
    this.categorie=categorie;
    this.prix=prix;
    this.desc=desc;
    this.qty=qty;
       
       
   }
   
   public int getid(){
       return id;
   }
   
   public String getnom(){
       return nom;
   }
   
   public int getcategorie(){
       return categorie;
   }
   
   public double getprix(){
       return prix;
   }
    
   public int getqty(){
       return qty;
   }
   
    public void setqty(int qty){
      this.qty=qty;
   }
   
   public String getdesc(){
       return desc;
   }
}
