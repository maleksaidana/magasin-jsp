
package demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Karim
 */
public class dataproduits {
    
    
    
    private Connection conn;
    private PreparedStatement cat,prod,all,allcat,admin,update,insert;
    private ResultSet result;
    private String url,user,pw;
    
    
    public dataproduits(String url,String user,String pw) throws ClassNotFoundException{
        this.url = url;
        this.user = user;
        this.pw = pw;   
        
           try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(this.url, this.user, this.pw);
           
            prod = conn.prepareStatement("select * from produit where  id = ? ;");
            cat = conn.prepareStatement("select * from categorie where id = ?;");
            all = conn.prepareStatement("select * from produit where categorie = ? ;");
            allcat = conn.prepareStatement("select * from categorie;");
            admin = conn.prepareStatement("select * from admin where id=? and password=? ;");
            update = conn.prepareStatement("update produit set nom=?,descr=?,qty=?, prix=? where id=?;");
             insert = conn.prepareStatement("insert into produit(nom,categorie,prix,descr,qty)values(?,?,?,?,?);");
         
        } 
                catch (SQLException ex)
        {
            Logger.getLogger(dataproduits.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
    }
    
    public categorie  getcategorie()    {
        categorie cat=null;
        try 
        {
            result = this.cat.executeQuery();
            
            while(result.next())
            {
              /*  cat= new legumes(result.getString("nom"),Double.valueOf(result.getString("prix")),
                        result.getString("type"));*/
            }
            
           
        }
        catch (SQLException ex)
        {
            Logger.getLogger(dataproduits.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         return cat;
    }
    
       
         public produit getproduit(String nom)    {
         produit leg=null;
         
        try 
        {
            this.prod.setString(1,nom);
            result = this.prod.executeQuery();
            
              while(result.next())
            {
               leg= new produit(Integer.parseInt(result.getString("id")),result.getString("nom"),Integer.parseInt(result.getString("categorie")),Double.valueOf(result.getString("prix")),result.getString("descr"),Integer.parseInt(result.getString("qty")));
            }
        }
        catch (SQLException ex)
        {
            
        }
          
        return leg;
    }  
       
       

 public ArrayList<produit> produits (int nom)  {
        ArrayList<produit> listleg = new ArrayList<produit>();
        try 
        {
            this.all.setInt(1,nom);
            result = this.all.executeQuery();
            
           while(result.next())
            {
              listleg.add(new produit(Integer.parseInt(result.getString("id")),result.getString("nom"),Integer.parseInt(result.getString("categorie")),Double.valueOf(result.getString("prix")),result.getString("descr"),Integer.parseInt(result.getString("qty"))));
              //  listleg.add(new produit(1,"malek",4,54)) ;       
            }
            
           
        }
        catch (Exception ex)
        {
            Logger.getLogger(dataproduits.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         return listleg;
    }

 
  public ArrayList<String> categories ()     {
        ArrayList<String> listleg = new ArrayList<String>();
        try 
        {
            result = this.all.executeQuery();
            
            while(result.next())
            {
               listleg.add((result.getString("nom")));
               
            }
            
           
        }
        catch (SQLException ex)
        {
            Logger.getLogger(dataproduits.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         return listleg;
    }
  
  
  
  
         public String getadmin(String id , String password){
         String res="0";
         
        try 
        {
            this.admin.setString(1,id);
            this.admin.setString(2,password);
            result = this.admin.executeQuery();
            
              while(result.next())
              {
               res= result.getString("id");   
            }
        }
        catch (SQLException ex)
        {
            return "0";
        }
          
        return res;
    }  
  
         
         public int upd(String nom,String prix,String desc,String qty,String id)        {
          int rowAffected = 0;     
        try 
        {
            this.update.setString(1,nom);
             this.update.setString(2,desc);
              this.update.setString(3,qty);
               this.update.setString(4,prix);
                this.update.setString(5,id);
            
            
            
            rowAffected = this.update.executeUpdate();
            
        }
        catch (SQLException ex)
        {
            return 454;
        }
          
        return rowAffected;
    }

      public int Inser(String nom,String prix,String cat,String desc,String qty)        {
          int rowAffected = 0;     
        try 
        {
            this.insert.setString(1,nom);
            this.insert.setString(2, cat);
            this.insert.setString(3,prix);
            this.insert.setString(4, desc);
            this.insert.setString(5,qty);
            
            rowAffected = this.insert.executeUpdate();
            
        }
        catch (SQLException ex)
        {
            return 404;
        }
          
        return rowAffected;
    }
         
         
    
}
