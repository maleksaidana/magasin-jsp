/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Karim
 */
@WebServlet(urlPatterns = {"/catserv"})
public class catserv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        String r="";
        dataproduits d=new  dataproduits("jdbc:mysql://localhost:3306/jsp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");
        
        String categorie = request.getParameter("cat");
         HttpSession session  = request.getSession() ;
        
          try (PrintWriter out = response.getWriter()) {
        if(categorie != null) 
        {
            
             ArrayList<produit> ar=d.produits(Integer.parseInt(categorie));
             for (produit l: ar) {
               
                 
                 r=r+"<li class='tab' id='"+l.getid()+"'> <time><img class=\"w-auto h-100\" src='produits/"+l.getid()+".jpg'></time> <div class='info'><h2 class='title'>"+l.getnom()+"</h2>  </div></li>";
                 	
										
						
					
                
             }
            //r=d.getproduit("1");
          //  out.println(r=d.getproduit("1"));
            
            request.setAttribute("u1", r);
            RequestDispatcher dispatch;
              if(session.getAttribute("login")==null) 
              dispatch = request.getRequestDispatcher("/categorie.jsp"); 
             else
              dispatch = request.getRequestDispatcher("/edit.jsp"); 
              
            dispatch.forward(request, response);
        
        }
          }
          
      /* 
         
            // TODO output your page here. You may use following sample code. 
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet catserv</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet catserv at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }*/
      
   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(catserv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(catserv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
