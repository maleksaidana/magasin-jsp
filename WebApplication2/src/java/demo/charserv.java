/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import demo.chariot;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Karim
 */
@WebServlet(urlPatterns = {"/charserv"})
public class charserv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
         
            /* TODO output your page here. You may use following sample code. */
            String r="";
            HttpSession session  = request.getSession() ;
            
          if(session.getAttribute("produit")!=null){
           
            chariot c= ((chariot)session.getAttribute("produit"));
            
            int cnt=0;
            for (produit p : c.list) {
                cnt++;
          r=r+" <div id='"+p.getid()+"' class='product'><div class='product-image'><img src='produits/"+p.getid()+".jpg'></div><div class='product-details'> <div class='product-title'>"+p.getnom()+"</div> <p class=\"product-description\">"+p.getdesc()+"</p></div> <div class='product-price'>"+p.getprix()+"</div><div class='product-quantity'> <input id="+p.getid()+" type='number' value="+p.getqty()+" min='1' max='20'> </div><div class='product-removal'><button type='button' id='"+p.getid()+"' class='remove-product'>Remove</button></div><div class='product-line-price'>"+p.getprix()*p.getqty()+"</div> </div><input type='hidden' name='item_name_"+cnt+"' value='"+p.getnom()+"'><input type='hidden' name='amount_"+cnt+"' value='"+p.getprix()+"'><input type='hidden' name='quantity_"+cnt+"' value='"+p.getqty()+"'> ";
                
            }
            
            r=r+"<input type='hidden' name='currency_code' value='CAD'><input type='hidden' name='return' value='www.facebook.com'><input type='hidden' name='cancel_return' value='www.facebook.com'>";
r=r+"<div class='totals'><div class='totals-item'><label>Subtotal</label><div class='totals-value' id='cart-subtotal'>"+c.gettotal()+"</div></div><div class='totals-item'><label>Tax (9%)</label><div class='totals-value' id='cart-tax'>"+c.gettotal()*0.09+"</div></div> <div class='totals-item totals-item-total'> <label>Grand Total</label> <div class='totals-value' id='cart-total'>"+(c.gettotal()+c.gettotal()*0.09)+"</div> </div> </div><button type='submit' class='checkout'>Checkout</button>";
          }   
          else{
              r="Le chariot est vide";
              session.setAttribute("prod",null);
          }
          
            request.setAttribute("produits", r);
               RequestDispatcher dispatch = request.getRequestDispatcher("/cart.jsp"); 
            dispatch.forward(request, response);
        }
                          
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
