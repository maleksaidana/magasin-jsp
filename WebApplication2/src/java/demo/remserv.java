/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Karim
 */
@WebServlet(name = "remserv", urlPatterns = {"/remserv"})
public class remserv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
       
          String type=  request.getParameter("type");
             int id= Integer.parseInt(request.getParameter("id"));        
            HttpSession session  = request.getSession() ;
            
            
            chariot c=((chariot)session.getAttribute("produit"));
            
          if(type.contains("r")){
              
              c.list.remove(c.getprod(id));
              session.setAttribute("prod",(session.getAttribute("prod").toString().replaceFirst(String.valueOf(id)+"/", "")));
               session.setAttribute("prod",(session.getAttribute("prod").toString().replaceFirst(String.valueOf(id), "")));
              if(c.list.size()>0)
               session.setAttribute("produit",c);
              else
              session.setAttribute("produit",null);
          }
          else if(type.contains("q")){
              int qty = Integer.parseInt(request.getParameter("qty"));
                c.getprod(id).setqty(qty);
                session.setAttribute("produit",c);
          }
          
          
          
            
          
          out.println(c.gettotal()); 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
