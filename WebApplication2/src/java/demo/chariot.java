
package demo;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Karim
 */
public class chariot {
    
 public   ArrayList<produit> list;
    
     
     public chariot(){
        list = new ArrayList<produit>();
     }
     
     
     double gettotal(){
         
         double total=0;
         for (produit p : list) {
         
             total=total+p.getprix()*p.getqty();
         
         }
         
         return (total);
     }
    
     produit getprod(int id){
         
          for (produit p : list) {
         
              if(id==p.getid())
             return p;
         
         }
         return null;
     }
     
}
