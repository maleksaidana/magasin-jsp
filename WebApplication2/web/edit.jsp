<%-- 
    Document   : edit
    Created on : 2020-04-27, 08:59:26
    Author     : Karim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
 <% 
            String u1 = (String)request.getAttribute("u1");
            
 %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <title>Edit</title>
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style>


/*menu*/


body {
    padding-top: 15px;
    padding-left: 65px;
}
.navbar-twitch {
	position: fixed;
	top: 0px;
	left: 0px;
	width: 50px;
	height: 100%;
	border-radius: 0px;
	border: 0px;
    z-index: 1030;
}
.navbar-twitch.open {
	width: 240px;
}
.navbar-twitch > .container {
	padding: 0px;
	width: 100%;
}
.navbar-twitch .navbar-header,
.navbar-twitch .navbar-brand {
	float: none;
	display: block;
	width: 100%;
	margin: 0px;
}
.navbar-twitch .navbar-brand {
    height: 50px;   
}
.navbar-twitch > .container .navbar-brand {
	padding: 0px;
	margin: 0px;
}
.navbar-twitch > .container .navbar-brand > .small-nav {
	display: block;
}
.navbar-twitch > .container .navbar-brand > .small-nav > span.logo,	
.navbar-twitch.open > .container .navbar-brand > .full-nav {
    display: block;
	font-weight: bold;
	padding: 15px 2px 15px 3px;
	font-size: 18pt;
}
.navbar-twitch.open > .container .navbar-brand {
	width: 100%;
	padding: 15px 15px 60px;
	text-align: center;
}
.navbar-twitch.navbar-inverse > .container .navbar-brand > .small-nav > span.logo {
	/*color: rgb(255, 255, 255);*/
}
.navbar-twitch .navbar-nav > li {
	float: none;
}
.navbar-twitch > .container .full-nav {
	display: none;
}
.navbar-twitch.open > .container .full-nav {
	display: block;
}
.navbar-twitch.open > .container .small-nav {
	display: none;
}
.navbar-twitch .navbar-collapse {
	padding: 0px;
}
.navbar-twitch .navbar-nav {
	float: none;
	margin: 0px;
}
.navbar-twitch .navbar-nav > li > a {
	padding: 0px;
}
.navbar-twitch .navbar-nav > li > a > span {
	display: block;
	font-size: 16pt;
	padding: 13px 15px 13px 12px;
}
.navbar-twitch .tooltip,
.navbar-twitch .tooltip .tooltip-inner {
	max-width: auto;
	white-space:nowrap;
}
.navbar-twitch-toggle {
	position: fixed;
	top: 5px;
	left: 55px;
}
.navbar-twitch ~ .navbar-twitch-toggle > .nav-open {
	display: inline-block;
}
.navbar-twitch ~ .navbar-twitch-toggle > .nav-close {
	display: none;
}
.navbar-twitch.open ~ .navbar-twitch-toggle {
	left: 245px;
}
.navbar-twitch.open ~ .navbar-twitch-toggle > .nav-open {
	display: none;
}
.navbar-twitch.open ~ .navbar-twitch-toggle > .nav-close {
	display: inline-block;
}


.btn-purple,
.btn-purple:hover,
.btn-purple:focus,
.btn-purple:Active {
    color: rgb(255, 255, 255);
    background-color: rgb(86, 61, 124);
    border-color: rgb(111, 84, 153);
}
.btn-purple:hover,
.btn-purple:focus,
.btn-purple:Active {
    background-color: rgb(111, 84, 153);
    border-color: rgb(86, 61, 124);   
}

.navbar {
    background-image: none !important;
}
.navbar-purple {
    background-color: rgb(86, 61, 124);
    border-color: rgb(111, 84, 153);
}
.navbar-purple .navbar-brand, 
.navbar-purple .navbar-nav > li > a, 
.navbar-purple .navbar-brand:hover, 
.navbar-purple .navbar-nav > li > a:hover, 
.navbar-purple .navbar-brand:focus, 
.navbar-purple .navbar-nav > li > a:focus {
    color: rgb(205, 191, 227);;
}
.navbar-purple .navbar-brand:hover, 
.navbar-purple .navbar-nav > li > a:hover, 
.navbar-purple .navbar-brand:focus, 
.navbar-purple .navbar-nav > li > a:focus,
.navbar-purple .navbar-nav > .active > a, 
.navbar-purple .navbar-nav > .active > a:hover, 
.navbar-purple .navbar-nav > .active > a:focus {
    background-color: rgb(111, 84, 153);
    color: rgb(255, 255, 255);
}




/*asba*/



#pinBoot {
  position: relative;
  max-width: 100%;
  width: 100%;
}
img {
  width: auto;
  margin:auto;
  height: 30vh;
}
.white-panel {
  position: absolute;
  background: white;
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);
  padding: 10px;
}
/*
stylize any heading tags withing white-panel below
*/

.white-panel h1 {
  font-size: 1em;
}
.white-panel h1 a {
  color: #A92733;
}
.white-panel:hover {
  box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.5);
  margin-top: -5px;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;
  transition: all 0.3s ease-in-out;
}


/*list prod*/

    .event-list {
		list-style: none;
		font-family: 'Lato', sans-serif;
		margin: 0px;
		padding: 0px;
	}
	.event-list > li {
		background-color: rgb(255, 255, 255);
		box-shadow: 0px 0px 5px rgb(51, 51, 51);
		box-shadow: 0px 0px 5px rgba(51, 51, 51, 0.7);
		padding: 0px;
		margin: 0px 0px 20px;
	}
	.event-list > li > time {
		display: inline-block;
		width: 100%;
		color: rgb(255, 255, 255);
		background-color: rgb(197, 44, 102);
		padding: 5px;
		text-align: center;
		text-transform: uppercase;
	}
	.event-list > li:nth-child(even) > time {
		background-color: rgb(165, 82, 167);
	}
	.event-list > li > time > span {
		display: none;
	}
	.event-list > li > time > .day {
		display: block;
		font-size: 70pt;
		font-weight: 100;
		line-height: 1;
	}
	.event-list > li time > .month {
		display: block;
		font-size: 24pt;
		font-weight: 900;
		line-height: 1;
	}
	.event-list > li > img {
		width: 100%;
	}
	.event-list > li > .info {
		padding-top: 5px;
		text-align: center;
	}
	.event-list > li > .info > .title {
		font-size: 17pt;
		font-weight: 700;
		margin: 0px;
	}
	.event-list > li > .info > .desc {
		font-size: 13pt;
		font-weight: 300;
		margin: 0px;
	}
	.event-list > li > .info > ul,
	.event-list > li > .social > ul {
		display: table;
		list-style: none;
		margin: 10px 0px 0px;
		padding: 0px;
		width: 100%;
		text-align: center;
	}
	.event-list > li > .social > ul {
		margin: 0px;
	}
	.event-list > li > .info > ul > li,
	.event-list > li > .social > ul > li {
		display: table-cell;
		cursor: pointer;
		color: rgb(30, 30, 30);
		font-size: 11pt;
		font-weight: 300;
        padding: 3px 0px;
	}
    .event-list > li > .info > ul > li > a {
		display: block;
		width: 100%;
		color: rgb(30, 30, 30);
		text-decoration: none;
	} 
    .event-list > li > .social > ul > li {    
        padding: 0px;
    }
    .event-list > li > .social > ul > li > a {
        padding: 3px 0px;
	} 
	.event-list > li > .info > ul > li:hover,
	.event-list > li > .social > ul > li:hover {
		color: rgb(30, 30, 30);
		background-color: rgb(200, 200, 200);
	}
	.facebook a,
	.twitter a,
	.google-plus a {
		display: block;
		width: 100%;
		color: rgb(75, 110, 168) !important;
	}
	.twitter a {
		color: rgb(79, 213, 248) !important;
	}
	.google-plus a {
		color: rgb(221, 75, 57) !important;
	}
	.facebook:hover a {
		color: rgb(255, 255, 255) !important;
		background-color: rgb(75, 110, 168) !important;
	}
	.twitter:hover a {
		color: rgb(255, 255, 255) !important;
		background-color: rgb(79, 213, 248) !important;
	}
	.google-plus:hover a {
		color: rgb(255, 255, 255) !important;
		background-color: rgb(221, 75, 57) !important;
	}

	@media (min-width: 768px) {
		.event-list > li {
			position: relative;
			display: block;
			width: 100%;
			height: 120px;
			padding: 0px;
		}
		.event-list > li > time,
		.event-list > li > img  {
			display: inline-block;
		}
		.event-list > li > time,
		.event-list > li > img {
			width: 120px;
			float: left;
		}
		.event-list > li > .info {
			background-color: rgb(245, 245, 245);
			overflow: hidden;
		}
		.event-list > li > time,
		.event-list > li > img {
			width: 120px;
			height: 120px;
			padding: 0px;
			margin: 0px;
		}
		.event-list > li > .info {
			position: relative;
			height: 120px;
			text-align: left;
			padding-right: 40px;
		}	
		.event-list > li > .info > .title, 
		.event-list > li > .info > .desc {
			padding: 0px 10px;
		}
		.event-list > li > .info > ul {
			position: absolute;
			left: 0px;
			bottom: 0px;
		}
		.event-list > li > .social {
			position: absolute;
			top: 0px;
			right: 0px;
			display: block;
			width: 40px;
		}
        .event-list > li > .social > ul {
            border-left: 1px solid rgb(230, 230, 230);
        }
		.event-list > li > .social > ul > li {			
			display: block;
            padding: 0px;
		}
		.event-list > li > .social > ul > li > a {
			display: block;
			width: 40px;
			padding: 10px 0px 9px;
		}
	}


.active{
    background-color:rgb(245, 225, 250)	 !important;
}
            
        </style>
        
    </head>
    <body>
        
        

<div class="navbar navbar-inverse navbar-twitch" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<span class="small-nav"> <span class="logo"> <B> </span> </span>
				<span class="full-nav"> < Bootsnipp > </span>
			</a>
		</div>
		<div class="">
			<ul class="nav navbar-nav">
			
			
						<li>
					<a href="index.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="Home"> 
													<span> <i style="font-size:24px" class="fa">&#127968;
                       </i></span> 
						</span>
						<span class="full-nav">Home </span>
					</a>
				</li>
			
				
                                <%  if(session.getAttribute("login")==null){ %>
                                
                                
                                <li>
					<a href="charserv">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="Chariot"> 
							<span> <i style="font-size:24px" class="fa">&#xf07a;</i></span> 
						</span>
						<span class="full-nav"> Chariot </span>
					</a>
				</li>
                                
                                
                                <li>
					<a href="login.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="login"> 
							<span> <i style="font-size:24px" class="fa">&#xf090;</i></span> 
						</span>
						<span class="full-nav"> Login </span>
					</a>
				</li>
                                                    
                               <% }else{ %>
                               
                               
                                  <li>
					<a href="add.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="add"> 
							<span> <i style="font-size:24px" class="fa">&#xf067;</i></span> 
						</span>
						<span class="full-nav"> Add </span>
					</a>
				</li>
                                
                               
                                                              
                               
                                 <li>
					<a href="logout">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="logout"> 
							<span> <i style="font-size:24px" class="fa">&#xf08b;</i></span> 
						</span>
						<span class="full-nav"> Logout </span>
					</a>
				</li>
                                
                                                                 
                               
                               
                               <%  } %>
		
			
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>
        <button type="button" class="btn btn-default btn-xs navbar-twitch-toggle">
	<span class="glyphicon glyphicon-chevron-right nav-open"></span>		
	<span class="glyphicon glyphicon-chevron-left nav-close"></span>
</button>

<div class="container">
	<div class="row">
		<h2>Bienvenue à l'épicerie du quartier</h2> 
        
	</div>
    <div class="row">
        
        <p>Vous etes connectes en mode administrateur</p>
        
    </div>
</div>
                               
                               
                               <div class="container">
        <div class="row">
            <div class="col-sm-6">
			
			
				<ul class="event-list">
					

				
<%=u1%>	
			
					
				</ul>
			
			
			</div>
			
			 <div class="col-sm-6">
			
                             <article  class="white-panel w-100 text-center">  <img  id="ved" src="http://i.imgur.com/xOIMvAe.jpg" alt="">
        <h4 class="edit" id="nom"><a href="#"></a></h4>
        <p class="edit" id="prix">Prix:</p>
		<p class="edit" id="qty"> Quantité:</p>
		<p class="edit" id="desc"> Description:</p>
  
                <p type="hidden" id="idd">
	 </article>
			
		
			
			
			</div>
			
		</div>
</div>






</body>

</html>
<script>


//var produit = {id:"def", nom:"def", qty:"def", prix:"def",img:"def" ,desc:"def"};

function getprod(c) {

        
       $.ajax({
            type: "post",
            url: "prodserv",
            data: { prod: c  },
            success: function (data) {

     $( "li .info" ).each(function( ) {
        $(this).removeClass("active");
           });
               
                             
           faza="#"+c+" .info";
              $(faza).addClass("active");
              var res = data.split("/");
              var img="produits/"+res[0]+".jpg";
              $("#idd").val(res[0]);
              $("#nom").html(res[1]);
              $("#qty").html("Quantité: "+res[5]);
              $("#prix").html("Prix: "+res[3]+ "$");
              $("#ved").attr("src",img );
              $("#desc").html("Description: "+res[4]);
              
              // produit = {id:res[0], nom:res[1], qty:res[5], prix:res[3],img:img ,desc:res[4]};
              
               window.produit = {
                id:res[0], 
                nom:res[1],
                qty:res[5],
                prix:res[3],
                img:img ,
                desc:res[4]
                };
            }
            

        }); 
        
        
}



var c = $(".tab").attr("id");
getprod(c);

$(".tab").click(function () {
    
    var c = $(this).attr("id");
  
  getprod(c);
       
    
    
 });
 


$('.edit').on('click',function() {
        
         var t = $(this).text();
         
          
          
         $(this).text('').append('<input />');
         $('input').val(t);
         $('input').focus();
          
         
              
         
       
});

$('body').on('blur','input',function() {
    $(this).parent().text($(this).val());
    var p=$("#prix").html().split("Prix: ")[1];
     window.produit = {
                id:$("#idd").val(), 
                nom:$("#nom").html(),
                qty:$("#qty").html().split("Quantité: ")[1],
                prix:p.substr(0,p.length-1),
                desc:$("#desc").html().split("Description: ")[1]
                };
                
                
                
     $.ajax({
            type: "post",
            url: "update",
           data: { id:produit.id, nom:produit.nom,qty:produit.qty,prix:produit.prix,desc:produit.desc},
            success: function (data) {

 
            }

        });        
                
    
});
 

</script>

                               
                               
                               
                               
    </body>
</html>
