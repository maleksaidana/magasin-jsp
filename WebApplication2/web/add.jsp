<%-- 
    Document   : add
    Created on : 2020-04-27, 14:23:56
    Author     : Karim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajouter Produit</title>
   
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style>

/* FontAwesome for working BootSnippet :> */

@import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
#team {
    background: #eee !important;
}

.btn-primary:hover,
.btn-primary:focus {
    background-color: #108d6f;
    border-color: #108d6f;
    box-shadow: none;
    outline: none;
}

.btn-primary {
    color: #fff;
    background-color: #007b5e;
    border-color: #007b5e;
}

section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}

#team .card {
    border: none;
    background: #ffffff;
}



.image-flip:hover .frontside,
.image-flip.hover .frontside {
cursor:pointer;
}

.mainflip {
    -webkit-transition: 1s;
    -webkit-transform-style: preserve-3d;
    -ms-transition: 1s;
    -moz-transition: 1s;
    -moz-transform: perspective(1000px);
    -moz-transform-style: preserve-3d;
    -ms-transform-style: preserve-3d;
    transition: 1s;
    transform-style: preserve-3d;
    position: relative;
}

.frontside {
    position: relative;
    -webkit-transform: rotateY(0deg);
    -ms-transform: rotateY(0deg);
    z-index: 2;
    margin-bottom: 30px;
}



.frontside,
.backside {
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transition: 1s;
    -webkit-transform-style: preserve-3d;
    -moz-transition: 1s;
    -moz-transform-style: preserve-3d;
    -o-transition: 1s;
    -o-transform-style: preserve-3d;
    -ms-transition: 1s;
    -ms-transform-style: preserve-3d;
    transition: 1s;
    transform-style: preserve-3d;
}

.frontside .card,
.backside .card {
    min-height: 312px;
}

.backside .card a {
    font-size: 18px;
    color: #007b5e !important;
}

.frontside .card .card-title,
.backside .card .card-title {
    color: #007b5e !important;
}

.frontside .card .card-body img {
    width: 120px;
    height: 120px;
    border-radius: 50%;
}

/*menu*/


body {
    padding-top: 15px;
    padding-left: 65px;
}
.navbar-twitch {
	position: fixed;
	top: 0px;
	left: 0px;
	width: 50px;
	height: 100%;
	border-radius: 0px;
	border: 0px;
    z-index: 1030;
}
.navbar-twitch.open {
	width: 240px;
}
.navbar-twitch > .container {
	padding: 0px;
	width: 100%;
}
.navbar-twitch .navbar-header,
.navbar-twitch .navbar-brand {
	float: none;
	display: block;
	width: 100%;
	margin: 0px;
}
.navbar-twitch .navbar-brand {
    height: 50px;   
}
.navbar-twitch > .container .navbar-brand {
	padding: 0px;
	margin: 0px;
}
.navbar-twitch > .container .navbar-brand > .small-nav {
	display: block;
}
.navbar-twitch > .container .navbar-brand > .small-nav > span.logo,	
.navbar-twitch.open > .container .navbar-brand > .full-nav {
    display: block;
	font-weight: bold;
	padding: 15px 2px 15px 3px;
	font-size: 18pt;
}
.navbar-twitch.open > .container .navbar-brand {
	width: 100%;
	padding: 15px 15px 60px;
	text-align: center;
}
.navbar-twitch.navbar-inverse > .container .navbar-brand > .small-nav > span.logo {
	/*color: rgb(255, 255, 255);*/
}
.navbar-twitch .navbar-nav > li {
	float: none;
}
.navbar-twitch > .container .full-nav {
	display: none;
}
.navbar-twitch.open > .container .full-nav {
	display: block;
}
.navbar-twitch.open > .container .small-nav {
	display: none;
}
.navbar-twitch .navbar-collapse {
	padding: 0px;
}
.navbar-twitch .navbar-nav {
	float: none;
	margin: 0px;
}
.navbar-twitch .navbar-nav > li > a {
	padding: 0px;
}
.navbar-twitch .navbar-nav > li > a > span {
	display: block;
	font-size: 16pt;
	padding: 13px 15px 13px 12px;
}
.navbar-twitch .tooltip,
.navbar-twitch .tooltip .tooltip-inner {
	max-width: auto;
	white-space:nowrap;
}
.navbar-twitch-toggle {
	position: fixed;
	top: 5px;
	left: 55px;
}
.navbar-twitch ~ .navbar-twitch-toggle > .nav-open {
	display: inline-block;
}
.navbar-twitch ~ .navbar-twitch-toggle > .nav-close {
	display: none;
}
.navbar-twitch.open ~ .navbar-twitch-toggle {
	left: 245px;
}
.navbar-twitch.open ~ .navbar-twitch-toggle > .nav-open {
	display: none;
}
.navbar-twitch.open ~ .navbar-twitch-toggle > .nav-close {
	display: inline-block;
}


.btn-purple,
.btn-purple:hover,
.btn-purple:focus,
.btn-purple:Active {
    color: rgb(255, 255, 255);
    background-color: rgb(86, 61, 124);
    border-color: rgb(111, 84, 153);
}
.btn-purple:hover,
.btn-purple:focus,
.btn-purple:Active {
    background-color: rgb(111, 84, 153);
    border-color: rgb(86, 61, 124);   
}

.navbar {
    background-image: none !important;
}
.navbar-purple {
    background-color: rgb(86, 61, 124);
    border-color: rgb(111, 84, 153);
}
.navbar-purple .navbar-brand, 
.navbar-purple .navbar-nav > li > a, 
.navbar-purple .navbar-brand:hover, 
.navbar-purple .navbar-nav > li > a:hover, 
.navbar-purple .navbar-brand:focus, 
.navbar-purple .navbar-nav > li > a:focus {
    color: rgb(205, 191, 227);;
}
.navbar-purple .navbar-brand:hover, 
.navbar-purple .navbar-nav > li > a:hover, 
.navbar-purple .navbar-brand:focus, 
.navbar-purple .navbar-nav > li > a:focus,
.navbar-purple .navbar-nav > .active > a, 
.navbar-purple .navbar-nav > .active > a:hover, 
.navbar-purple .navbar-nav > .active > a:focus {
    background-color: rgb(111, 84, 153);
    color: rgb(255, 255, 255);
}

</style>

</head>

<body>


<div class="navbar navbar-inverse navbar-twitch" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<span class="small-nav"> <span class="logo"> <B> </span> </span>
				<span class="full-nav"> < Bootsnipp > </span>
			</a>
		</div>
		<div class="">
			<ul class="nav navbar-nav">
			
			
						<li>
					<a href="index.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="Home"> 
													<span> <i style="font-size:24px" class="fa">&#127968;
                       </i></span> 
						</span>
						<span class="full-nav">Home </span>
					</a>
				</li>
			
			                                
                                <%  if(session.getAttribute("login")==null){ %>
                                
                                <li>
					<a href="charserv">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="Chariot"> 
							<span> <i style="font-size:24px" class="fa">&#xf07a;</i></span> 
						</span>
						<span class="full-nav"> Chariot </span>
					</a>
				</li>
                                
                                <li>
					<a href="login.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="login"> 
							<span> <i style="font-size:24px" class="fa">&#xf090;</i></span> 
						</span>
						<span class="full-nav"> Login </span>
					</a>
				</li>
                                                    
                               <% }else{ %>
                               
                                 <li>
					<a href="add.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="add"> 
							<span> <i style="font-size:24px" class="fa">&#xf067;</i></span> 
						</span>
						<span class="full-nav"> Add </span>
					</a>
				</li>
                                                          
                               
                                 <li>
					<a href="logout">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="logout"> 
							<span> <i style="font-size:24px" class="fa">&#xf08b;</i></span> 
						</span>
						<span class="full-nav"> Logout </span>
					</a>
				</li>
                                
                                                                 
                               
                               
                               <%  } %>
		
			
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>
<button type="button" class="btn btn-default btn-xs navbar-twitch-toggle">
	<span class="glyphicon glyphicon-chevron-right nav-open"></span>		
	<span class="glyphicon glyphicon-chevron-left nav-close"></span>
</button>

<div class="container">
	<div class="row">
		<h2>Bienvenue à l'épicerie du quartier</h2> 
        
	</div>
    <div class="row">
        
       <%  if(session.getAttribute("login")==null){ %>	<p>Vous pouvez faire vos achats ici en ligne</p> <%}else{%> 	<p>Vous etes connecté en mode adminstrateur</p> <%}%>
        
    </div>
</div>
        
        <div class="container">
  <h2>Ajouter Un Produit</h2>
  <form class="form-horizontal" action="ajout">
    <div class="form-group">
      <label class="control-label col-sm-2" >Nom:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="nom" placeholder="Nom" name="nom" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" >Prix:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" id="prix" placeholder="prix" name="prix" required>
      </div>
    </div>
      <div class="form-group">
      <label class="control-label col-sm-2" >Description:</label>
      <div class="col-sm-10">          
          <textarea type="text" class="form-control" id="desc" placeholder="Description" required name="desc"></textarea>
      </div>
    </div>
        <div class="form-group">
            <label class="control-label col-sm-2" >quantité:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" id="qty" placeholder="Quantite" name="qty" required>
      </div>
    </div>
        <div class="form-group">
      <label class="control-label col-sm-2" >Categorie:</label>
      <div class="col-sm-10">          
        <input type="number" class="form-control" id="cat" placeholder="Categorie" name="cat" required>
      </div>
    </div>
   
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Add</button>
      </div>
    </div>
  </form>
</div>

        
    </body>
</html>
