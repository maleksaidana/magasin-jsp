<%-- 
    Document   : login
    Created on : 2020-04-27, 08:23:26
    Author     : Karim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        
        
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style>


/*menu*/


body {
    padding-top: 15px;
    padding-left: 65px;
}
.navbar-twitch {
	position: fixed;
	top: 0px;
	left: 0px;
	width: 50px;
	height: 100%;
	border-radius: 0px;
	border: 0px;
    z-index: 1030;
}
.navbar-twitch.open {
	width: 240px;
}
.navbar-twitch > .container {
	padding: 0px;
	width: 100%;
}
.navbar-twitch .navbar-header,
.navbar-twitch .navbar-brand {
	float: none;
	display: block;
	width: 100%;
	margin: 0px;
}
.navbar-twitch .navbar-brand {
    height: 50px;   
}
.navbar-twitch > .container .navbar-brand {
	padding: 0px;
	margin: 0px;
}
.navbar-twitch > .container .navbar-brand > .small-nav {
	display: block;
}
.navbar-twitch > .container .navbar-brand > .small-nav > span.logo,	
.navbar-twitch.open > .container .navbar-brand > .full-nav {
    display: block;
	font-weight: bold;
	padding: 15px 2px 15px 3px;
	font-size: 18pt;
}
.navbar-twitch.open > .container .navbar-brand {
	width: 100%;
	padding: 15px 15px 60px;
	text-align: center;
}
.navbar-twitch.navbar-inverse > .container .navbar-brand > .small-nav > span.logo {
	/*color: rgb(255, 255, 255);*/
}
.navbar-twitch .navbar-nav > li {
	float: none;
}
.navbar-twitch > .container .full-nav {
	display: none;
}
.navbar-twitch.open > .container .full-nav {
	display: block;
}
.navbar-twitch.open > .container .small-nav {
	display: none;
}
.navbar-twitch .navbar-collapse {
	padding: 0px;
}
.navbar-twitch .navbar-nav {
	float: none;
	margin: 0px;
}
.navbar-twitch .navbar-nav > li > a {
	padding: 0px;
}
.navbar-twitch .navbar-nav > li > a > span {
	display: block;
	font-size: 16pt;
	padding: 13px 15px 13px 12px;
}
.navbar-twitch .tooltip,
.navbar-twitch .tooltip .tooltip-inner {
	max-width: auto;
	white-space:nowrap;
}
.navbar-twitch-toggle {
	position: fixed;
	top: 5px;
	left: 55px;
}
.navbar-twitch ~ .navbar-twitch-toggle > .nav-open {
	display: inline-block;
}
.navbar-twitch ~ .navbar-twitch-toggle > .nav-close {
	display: none;
}
.navbar-twitch.open ~ .navbar-twitch-toggle {
	left: 245px;
}
.navbar-twitch.open ~ .navbar-twitch-toggle > .nav-open {
	display: none;
}
.navbar-twitch.open ~ .navbar-twitch-toggle > .nav-close {
	display: inline-block;
}


.btn-purple,
.btn-purple:hover,
.btn-purple:focus,
.btn-purple:Active {
    color: rgb(255, 255, 255);
    background-color: rgb(86, 61, 124);
    border-color: rgb(111, 84, 153);
}
.btn-purple:hover,
.btn-purple:focus,
.btn-purple:Active {
    background-color: rgb(111, 84, 153);
    border-color: rgb(86, 61, 124);   
}

.navbar {
    background-image: none !important;
}
.navbar-purple {
    background-color: rgb(86, 61, 124);
    border-color: rgb(111, 84, 153);
}
.navbar-purple .navbar-brand, 
.navbar-purple .navbar-nav > li > a, 
.navbar-purple .navbar-brand:hover, 
.navbar-purple .navbar-nav > li > a:hover, 
.navbar-purple .navbar-brand:focus, 
.navbar-purple .navbar-nav > li > a:focus {
    color: rgb(205, 191, 227);;
}
.navbar-purple .navbar-brand:hover, 
.navbar-purple .navbar-nav > li > a:hover, 
.navbar-purple .navbar-brand:focus, 
.navbar-purple .navbar-nav > li > a:focus,
.navbar-purple .navbar-nav > .active > a, 
.navbar-purple .navbar-nav > .active > a:hover, 
.navbar-purple .navbar-nav > .active > a:focus {
    background-color: rgb(111, 84, 153);
    color: rgb(255, 255, 255);
}


/*login*/

.wrapper {
  display: flex;
  align-items: center;
  flex-direction: column; 

  width: 100%;
  min-height: 100%;
  padding: 20px;
}

#formContent {
  -webkit-border-radius: 10px 10px 10px 10px;
  border-radius: 10px 10px 10px 10px;
  background: #fff;
  padding: 30px;
  width: 90%;
  max-width: 450px;
  position: relative;
  padding: 0px;
  -webkit-box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  text-align: center;
}

#formFooter {
  background-color: #f6f6f6;
  border-top: 1px solid #dce8f1;
  padding: 25px;
  text-align: center;
  -webkit-border-radius: 0 0 10px 10px;
  border-radius: 0 0 10px 10px;
}



/* TABS */

h2.inactive {
  color: #cccccc;
}

h2.active {
  color: #0d0d0d;
  border-bottom: 2px solid #5fbae9;
}



/* FORM TYPOGRAPHY*/

input[type=button], input[type=submit], input[type=reset]  {
  background-color: #56baed;
  border: none;
  color: white;
  padding: 15px 80px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  text-transform: uppercase;
  font-size: 13px;
  -webkit-box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
  box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
  -webkit-border-radius: 5px 5px 5px 5px;
  border-radius: 5px 5px 5px 5px;
  margin: 5px 20px 40px 20px;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;
  transition: all 0.3s ease-in-out;
}

input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover  {
  background-color: #39ace7;
}

input[type=button]:active, input[type=submit]:active, input[type=reset]:active  {
  -moz-transform: scale(0.95);
  -webkit-transform: scale(0.95);
  -o-transform: scale(0.95);
  -ms-transform: scale(0.95);
  transform: scale(0.95);
}

input {
  background-color: #f6f6f6;
  border: none;
  color: #0d0d0d;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 5px;
  width: 85%;
  border: 2px solid #f6f6f6;
  -webkit-transition: all 0.5s ease-in-out;
  -moz-transition: all 0.5s ease-in-out;
  -ms-transition: all 0.5s ease-in-out;
  -o-transition: all 0.5s ease-in-out;
  transition: all 0.5s ease-in-out;
  -webkit-border-radius: 5px 5px 5px 5px;
  border-radius: 5px 5px 5px 5px;
}

input[type=text]:focus {
  background-color: #fff;
  border-bottom: 2px solid #5fbae9;
}

input[type=text]:placeholder {
  color: #cccccc;
}



/* ANIMATIONS */

/* Simple CSS3 Fade-in-down Animation */
.fadeInDown {
  -webkit-animation-name: fadeInDown;
  animation-name: fadeInDown;
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

@-webkit-keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

@keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

/* Simple CSS3 Fade-in Animation */
@-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

.fadeIn {
  opacity:0;
  -webkit-animation:fadeIn ease-in 1;
  -moz-animation:fadeIn ease-in 1;
  animation:fadeIn ease-in 1;

  -webkit-animation-fill-mode:forwards;
  -moz-animation-fill-mode:forwards;
  animation-fill-mode:forwards;

  -webkit-animation-duration:1s;
  -moz-animation-duration:1s;
  animation-duration:1s;
}

.fadeIn.first {
  -webkit-animation-delay: 0.4s;
  -moz-animation-delay: 0.4s;
  animation-delay: 0.4s;
}

.fadeIn.second {
  -webkit-animation-delay: 0.6s;
  -moz-animation-delay: 0.6s;
  animation-delay: 0.6s;
}

.fadeIn.third {
  -webkit-animation-delay: 0.8s;
  -moz-animation-delay: 0.8s;
  animation-delay: 0.8s;
}

.fadeIn.fourth {
  -webkit-animation-delay: 1s;
  -moz-animation-delay: 1s;
  animation-delay: 1s;
}

/* Simple CSS3 Fade-in Animation */
.underlineHover:after {
  display: block;
  left: 0;
  bottom: -10px;
  width: 0;
  height: 2px;
  background-color: #56baed;
  content: "";
  transition: width 0.2s;
}

.underlineHover:hover {
  color: #0d0d0d;
}

.underlineHover:hover:after{
  width: 100%;
}



/* OTHERS */

*:focus {
    outline: none;
} 

#icon {
  width:60%;
}


</style>

</head>

        
        
    </head>
    <body>
        


<div class="navbar navbar-inverse navbar-twitch" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">
				<span class="small-nav"> <span class="logo"> <B> </span> </span>
				<span class="full-nav"> < Bootsnipp > </span>
			</a>
		</div>
		<div class="">
			<ul class="nav navbar-nav">
			
			
						<li>
					<a href="index.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="Home"> 
													<span> <i style="font-size:24px" class="fa">&#127968;
                       </i></span> 
						</span>
						<span class="full-nav">Home </span>
					</a>
				</li>
			
				
                                <%  if(session.getAttribute("login")==null){ %>
                                
                                <li>
					<a href="charserv">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="Chariot"> 
							<span> <i style="font-size:24px" class="fa">&#xf07a;</i></span> 
						</span>
						<span class="full-nav"> Chariot </span>
					</a>
				</li>
                                
                                <li>
					<a href="login.jsp">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="login"> 
							<span> <i style="font-size:24px" class="fa">&#xf090;</i></span> 
						</span>
						<span class="full-nav"> Login </span>
					</a>
				</li>
                                                    
                               <% }else{ %>
                               
                                                      
                               
                                 <li>
					<a href="logout">
						<span class="small-nav" data-toggle="tooltip" data-placement="right" title="logout"> 
							<span> <i style="font-size:24px" class="fa">&#xf08b;</i></span> 
						</span>
						<span class="full-nav"> Logout </span>
					</a>
				</li>
                                
                                                                 
                               
                               
                               <%  } %>
		
			
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div>
<button type="button" class="btn btn-default btn-xs navbar-twitch-toggle">
	<span class="glyphicon glyphicon-chevron-right nav-open"></span>		
	<span class="glyphicon glyphicon-chevron-left nav-close"></span>
</button>

<div class="container">
	<div class="row">
		<h2>Bienvenue à l'épicerie du quartier</h2> 
        
	</div>
    <div class="row">
        
        <p>Vous pouvez faire vos achats ici en ligne</p>
        
    </div>
</div>


<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <p> -Only Administrator- </p>
    </div>

    <!-- Login Form -->
    
      <input  type="text" id="username" class="fadeIn second" name="login" placeholder="login">
      <input id="password" class="fadeIn third" name="login" placeholder="password" type="password">
      <input id="login" type="button" class="fadeIn fourth" value="Log In">
  

 

  </div>
</div>
    </body>
</html>


<script>
$(document).ready(function(){
     
$("#login").click(function() {

var login = $("#username").val();
var password = $("#password").val();


    $.ajax({
        type: "post",
	url: "verif",
	data: {id:login,password:password},
		
        success: function (data) {

            if (data != "0")
             window.location.href = "index.jsp";
            else
                alert("Mauvais Username et/ou Mot De passe");


			
        }
		
    });

});

});

</script>